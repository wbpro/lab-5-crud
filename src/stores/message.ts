import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const showMessage = (msg: string) => {
    message.value = msg;
    isShow.value = true;
  };
  const closeMessage = () => {
    message.value = "";
    isShow.value = false;
  };
  return { isShow, message, timeout, showMessage, closeMessage };
});
